const socket = io()
enchant()

window.onload = function () {
    game = new Game(320, 320)
    game.fps = 15
    game.preload('chara0.gif', 'icon0.gif', 'galaxy.png')
    game.onload = function () {
        game.replaceScene(playScene())
    }
    game.start()
}

//Title--------------------------------------------------------------------------
var titleScene = function () {
    scene = new Scene()
    var title = new Label()
    title.text = "TestStart"
    title.x = 180
    title.y = 180
    title.color = "red"
    title.addEventListener(Event.TOUCH_START, function (e) {
        game.replaceScene(playScene())
    })
    scene.addChild(title)
    return scene
}
//Play---------------------------------------------------------------------------------
var playScene = function () {
    scene = new Scene()

    map = new Map(16, 16)
    map.image = game.assets["galaxy.png"]
    map.loadData.apply(map, mapLoader.map);
    scene.addChild(map)

    var name = Math.floor(Math.random() * 10000)
    socket.emit("name", name)
    socket.emit("position", { x: 0, y: 0, frame: 0 })
    var chara_group = new Group();
    var bullet_group = new Group();
    var planet_group = new Group()

    var player = new MainPlayer(name)
    player.addEventListener("touchstart",function(){
        var id = this.chara_name + (Math.floor(Math.random() * 10000))
        socket.emit("shooting", { x: this.x, y: this.y, name: this.chara_name, id: id, direction: this.direction })
        var new_bullet=new MyBullet(this.x,this.y,this.chara_name,id,this.direction)
        bullet_group.addChild(new_bullet)
    })
    chara_group.addChild(player)

    socket.on("hit:" + name, (params) => {
        socket.emit("disconnect")
        game.replaceScene(gameOver())
    })

    //Socket----------------------------------------------------------
    //新しいプレイヤーのログイン
    socket.on("name", (txt) => {
        var other_name = txt;
        var other_player = new Player(other_name)
        chara_group.addChild(other_player)
        //このプレイヤーの移動
        socket.on("position:" + other_name, (pos) => {
            other_player.x = pos.x;
            other_player.y = pos.y;
            other_player.frame = pos.frame;
        })
        //このプレーヤーの切断
        socket.on("disconnect:" + other_name, () => {
            chara_group.removeChild(other_player)
            delete other_player;
        })
        //ヒット処理
        socket.on("hit:" + other_name, (params) => {
            chara_group.removeChild(other_player)
            delete other_player;
        })
    })
    //新しい弾丸の描画
    socket.on("shoot", (bullet) => {
        var new_bullet = new EnemyBullet(bullet.x, bullet.y, bullet.name, bullet.id, bullet.direction)
        bullet_group.addChild(new_bullet)
    })
    /*
    //星の生成
    socket.on("planet_generate", (p_pos) => {
        var new_planet = new Planet(p_pos.x, p_pos.y, p_pos.frame)
        planet_group.addChild(new_planet)
    })*/
    scene.addEventListener('enterframe', function () {
        MainPlayer.intersect(EnemyBullet).forEach(function (pair) {
            var params = {
                chara_name: pair[0].chara_name,
                bullet_name: pair[1].name
            }
            socket.emit("hit", params)
            game.replaceScene(gameOver())
        })
    })
    var stage = new Group()
    stage.addChild(chara_group)
    stage.addChild(bullet_group)
    stage.addChild(planet_group)
    scene.addChild(stage)
    return scene
}
//Gameover-------------------------------------------------------------------------------------
var gameOver = function () {
    scene = new Scene()
    txt = new Label()
    txt.text = "gameover"
    txt.x = 100
    txt.y = 100
    scene.addChild(txt)
    return scene
}

//Class----------------------------------------------------------------------------------------
var Player = Class.create(Sprite, {
    initialize: function (chara_name) {
        Sprite.call(this, 32, 32);
        this.x = 0;
        this.y = 0;
        this.frame = 0;
        this.direction = "down";
        this.chara_name = chara_name;
        this.image = game.assets['chara0.gif'];
        this.isMoving = false;
        this.hp = 50;
        this.energy = 0;
    },
})

var MainPlayer = Class.create(Player, {
    initialize: function (chara_name) {
        Player.call(this, chara_name)
        /*
        this.addEventListener('touchstart', function () {
            var id = this.chara_name + (Math.floor(Math.random() * 10000))
            socket.emit("shooting", { x: this.x, y: this.y, name: this.chara_name, id: id, direction: this.direction })
        })*/
    },
    onenterframe: function () {
        if (game.input.left) {
            this.direction = "left"
            this.frame = 11;
            this.x -= 3
            socket.emit("position", { x: this.x, y: this.y, frame: this.frame });
        } else if (game.input.right) {
            this.direction = "right"
            this.frame = 20;
            this.x += 3
            socket.emit("position", { x: this.x, y: this.y, frame: this.frame });
        } else if (game.input.up) {
            this.direction = "up"
            this.frame = 29;
            this.y -= 3;
            socket.emit("position", { x: this.x, y: this.y, frame: this.frame });
        } else if (game.input.down) {
            this.direction = "down"
            this.frame = 0
            this.y += 3
            socket.emit("position", { x: this.x, y: this.y, frame: this.frame });
        }
        if (this.hp == 0) {
            socket.emit("disconnect")
            game.replaceScene(gameOver())
        }
    },
})

var Enemy = Class.create(Sprite, {
    initialize: function () {
        Sprite.call(32, 32);
        this.x = 0;
        this.y = 0;
    },
    onenterframe: function () {

    }
})

var Bullet = Class.create(Sprite, {
    initialize: function (x, y, name, id, direction) {
        Sprite.call(this, 16, 16);
        this.x = x;
        this.y = y;
        this.name = name;
        this.id = id
        this.direction = direction
        this.image = game.assets["icon0.gif"]
        this.frame = bullet_frame(direction)
    },
    onenterframe: function () {
        if (this.direction === "left") {
            this.x -= 17
        } else if (this.direction === "right") {
            this.x += 17
        } else if (this.direction === "up") {
            this.y -= 17
        } else if (this.direction == "down") {
            this.y += 17
        }
    },
})

var MyBullet = Class.create(Bullet, {
    initialize: function (x, y, name, id, direction) {
        Bullet.call(this, x, y, name, id, direction)
    }
})

var EnemyBullet = Class.create(Bullet, {
    initialize: function (x, y, name, id, direction) {
        Bullet.call(this, x, y, name, id, direction)
    }
})


//bulletの画像
function bullet_frame(direction) {
    switch (direction) {
        case "left":
            return 50
        case "right":
            return 54
        case "up":
            return 48
        case "down":
            return 52
    }
}

var Planet = Class.create(Sprite, {
    initialize: function (x, y, frame) {
        Sprite.call(this, 64, 64)
        this.x = x;
        this.y = y;
        this.frame = frame;
        this.hp = 100;
    },

})
