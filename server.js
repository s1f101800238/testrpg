const express = require("express");
const app = express();
const http = require('http').Server(app)
const io = require('socket.io')(http)
const PORT = process.env.PORT || 7000;

app.use(express.static('public'))

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/public/index.html')
})

http.listen(PORT, () => {
    console.log('Server started on port:' + PORT);
});

var player_list = new Array()

var bullet_list = new Array()

var planet_list = new Array()

io.on("connection", function (socket) {

    var player = {
        chara_name: "",
        x: 0,
        y: 0,
        frame: 0,
        direction: 0,
    }

    socket.on("name", (txt) => {
        player.chara_name = txt
        socket.broadcast.emit("name", player.chara_name)
        for (var i in player_list) {
            var chara = player_list[i]
            socket.emit("name", chara.chara_name)
            socket.emit("position:" + chara.chara_name, { x: chara.x, y: chara.y, frame: chara.frame })
        }
        player_list[player.chara_name] = player
    })

    socket.on("position", (pos) => {
        player.x = pos.x
        player.y = pos.y
        player.frame = pos.frame
        socket.broadcast.emit("position:" + player.chara_name, pos)
    })
    socket.on("disconnect", () => {
        socket.broadcast.emit("disconnect:" + player.chara_name)
        delete player_list[player.chara_name]
    })

    socket.on("shooting", (bullet_data) => {
        var bullet = {
            x: bullet_data.x,
            y: bullet_data.y,
            name: bullet_data.name,
            id: bullet_data.id,
            direction: bullet_data.direction
        }
        socket.broadcast.emit("shoot", bullet)
        bullet_list[bullet_data.id] = bullet
    })

    socket.on("hit", (params) => {
        var chara_name = params.chara_name
        io.sockets.emit("hit:" + chara_name, params)
        delete player_list[player.chara_name]
    })
})
/*
setInterval(function () {
    var p_x = 0
    var p_y = 0
    var p_pos = {
        x: 0,
        y: 0,
        frame: 0,
    }
    io.sockets.emit("planet_generate")
}, 1000)
*/
